<?php
class BTeodor_ImportProduse_Adminhtml_ImportController
    extends Mage_Adminhtml_Controller_Action
{
    private $storeId = 1;
    private $websiteId = 1;
    private $attributeSetId = 9;
    protected $file_upload_path = '';
    protected $file_name = '';
    protected $file_fullname = '';
    protected $file_path = '';
    protected $objPHPExcel = NULL;
    protected $produse = array();
    protected $produse_gasite = 0;
    protected $category = false;
    private $err = NULL;
//    protected $file_path = '';
//    protected $file_path = '';

    public function indexAction()
    {
        // instantiate the grid container
        $brandBlock = $this->getLayout()
            ->createBlock('bteodor_importproduse_adminhtml/import');
        
        // add the grid container as the only item on this page
        $this->loadLayout()
            ->_addContent($brandBlock)
            ->renderLayout();
    }
    
    /**
     * This action handles both viewing and editing of existing brands.
     */
    public function editAction()
    {
        // process $_POST data if the form was submitted
        if ($postData = $this->getRequest()->getPost('form_key')) {
            try {
//                var_dump($_FILES);
                if(!$this->_upload_file()) $this->_error("Fisierul nu a fost uploadat");

                if(!$this->_load_excel()) $this->_error("Fisierul nu a putut fi citit");

                if(!$this->_get_excel_data()) $this->_error("Nu a fost gasit nici un produs in fisier");

                for($i=0; $i<$this->produse_gasite; $i++) {
                    $produs = $this->produse[$i];

                    if(!$category = $this->_get_category($produs->categorie)) $this->_error("Nu s-a putut crea categoria");

                    if($this->_get_product($produs))
                        $this->_error("Produsul exista");
                    else
                        if(!$this->_add_product($produs,$category)) $this->_error("Nu s-a putut crea produsul");

                }

                if(!$this->_add_log()) $this->_error("Logul nu a putut fi inregistrat");

                $this->_getSession()->addSuccess(
                    $this->__('The brand has been saved.')
                );

                return $this->_redirect(
                    'bteodor_importproduse_admin/import'
                );

            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }
        
        // instantiate the form container
        $brandEditBlock = $this->getLayout()->createBlock(
            'bteodor_importproduse_adminhtml/import_edit'
        );
        
        // add the form container as the only item on this page
        $this->loadLayout()
            ->_addContent($brandEditBlock)
            ->renderLayout();
    }
    protected function _upload_file() {
        $this->err = NULL;
        if (isset($_FILES['fisier']['name']) && $_FILES['fisier']['name'] != '') {
            try
            {
                $this->file_upload_path = Mage::getBaseDir().DS.'uploads'.DS.'import'.DS;  //desitnation directory
                $this->file_name = $_FILES['fisier']['name']; //file name
                $this->file_fullname = $this->file_upload_path.$this->file_name;
                $uploader = new Varien_File_Uploader($_FILES['fisier']); //load class
                $uploader->setAllowedExtensions(array('CSV','csv','xls','XLS')); //Allowed extension for file
                $uploader->setAllowCreateFolders(true); //for creating the directory if not exists
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $uploader->save($this->file_upload_path, $this->file_name); //save the
                return true;
            }
            catch (Exception $e)
            {
                Mage::log($e->getMessage());
                $this->err = $e->getMessage();
                return false;
            }
        }
        return false;
    }
    protected function _load_excel() {
        $this->err = NULL;
        $includePath = Mage::getBaseDir(). "/lib/PHPExcel";
        set_include_path(get_include_path() . PS . $includePath);

        try {
            $inputFileType = PHPExcel_IOFactory::identify($this->file_fullname);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $this->objPHPExcel = $objReader->load($this->file_fullname);
            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            $this->err = 'Eroare citire fisier '.pathinfo($this->file_fullname, PATHINFO_BASENAME).': '.$e->getMessage();
            return false;
        }
    }
    protected function _get_excel_data() {
        $this->err = NULL;
        //  Get worksheet dimensions
        $sheet = $this->objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++) {
            $produs = new stdClass();
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
//                    foreach($rowData[0] as $k=>$v) {
//                        echo "Row: " . $row . "- Col: " . ($k + 1) . " = " . $v . "<br />";
//
//
//                    }
            $categorie = $rowData[0][1];
            $categorie_arr = explode(" ",$categorie);
            $cod = array_pop($categorie_arr);
            $categorie = implode(" ",$categorie_arr);
            $um = $rowData[0][2];
            $pret = $rowData[0][5];

            $produs->nume = $rowData[0][1];
            $produs->categorie = $categorie;
            $produs->sku = $cod;
            $produs->um = $um;
            $produs->pret = $pret;

            if($produs->nume !== NULL && $produs->nume != ''
                && $produs->categorie !== NULL && $produs->categorie != ''
                && $produs->sku !== NULL && $produs->sku != ''
                && $produs->um !== NULL && $produs->um != ''
                && $produs->pret !== NULL && $produs->pret != '')
            $this->produse[] = $produs;
        }
//        var_dump($this->produse);
//        die();
        $this->produse_gasite = count($this->produse);
        if($this->produse_gasite > 0) return true;
        else return false;
    }
    protected function _get_category($categorie) {
        $this->err = NULL;

        $category = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToFilter('is_active', true)
            ->addAttributeToFilter('name', $categorie)
            ->getFirstItem()    // Assuming your category names are unique ??
        ;
        if (null !== $category->getId()) {
            return $category;
        } else {
            return $this->_add_category($categorie);
        }
    }
    protected function _add_category($categorie) {
        $this->err = NULL;
//        $slug = Mage::getModel('catalog/product_url')->formatUrlKey($produs['categorie']);
//                        $storeId = Mage::app()->getStore()->getStoreId();

//                        var_dump($storeId);
        $parentId = Mage::app()->getStore($this->storeId)->getRootCategoryId();
//                        var_dump($parentId);
//                        die();
        $parentCategory = Mage::getModel('catalog/category')->load($parentId);
        $path = $parentCategory->getPath();
        $data = array(
            'general' => array(
                'path' => $path,
                'name' => $categorie,
                'is_active' => '1',
                'description' => 'Descriere '.$categorie,
                'meta_title' => $categorie,
                'meta_keywords' => $categorie,
                'meta_description' => $categorie,
                'include_in_menu' => '1',
                'display_mode' => 'PRODUCTS',
                'landing_page' => '',
                'is_anchor' => '0',
                'custom_use_parent_settings' => '0',
                'custom_apply_to_products' => '0',
                'custom_design' => '',
                'custom_design_from' => '',
                'custom_design_to' => '',
                'page_layout' => '',
                'custom_layout_update' => '',
            ),
            'use_config' => array(
                'available_sort_by',
                'default_sort_by',
                'filter_price_range',
            ),
            'use_default' => array(
                'available_sort_by',
                'default_sort_by',
                'filter_price_range',
            ),
            'page' => 1,
            'limit' => 20,
            'in_category' => '',
            'entity_id' => '',
            'name' => '',
            'sku' => ''
        );
//                        var_dump($data);
        try{
//                            var_dump($data['use_config']);
            $category = Mage::getModel('catalog/category');
            $category->setStoreId($this->storeId);
//                            $category->setName($produs['categorie']);
//                            $category->setUrlKey($slug);
//                            $category->setIsActive(1);
//                            $category->setDisplayMode('PRODUCTS');
//                            $category->setIsAnchor(1); //for active achor
//                            $category->setStoreId($storeId);

//                            $category->setPath($parentCategory->getPath());
//                            $category->save();
            $category->addData($data['general']);
            if (!$category->getId()) {
                $parentCategory = Mage::getModel('catalog/category')->load($parentId);
                $category->setPath($parentCategory->getPath());
            }

            /**
             * Check "Use Default Value" checkboxes values
             */
            if ($useDefaults = $data['use_default']) {
                foreach ($useDefaults as $attributeCode) {
                    $category->setData($attributeCode, false);
                }
            }
            /**
             * Process "Use Config Settings" checkboxes
             */
//                            var_dump($data['use_config']);
            if ($useConfig = $data['use_config']) {
                foreach ($useConfig as $attributeCode) {
                    $category->setData($attributeCode, null);
                }
            }
            $category->setAttributeSetId($category->getDefaultAttributeSetId());

            $category->setData("use_post_data_config", $data['use_config']);

            try {
                $validate = $category->validate();
                if ($validate !== true) {
                    foreach ($validate as $code => $error) {
                        if ($error === true) {
//                            echo $code;
                            Mage::throwException(Mage::helper('catalog')->__('Attribute "%s" is required.', $category->getResource()->getAttribute($code)->getFrontend()->getLabel()));
                        }
                        else {
                            Mage::throwException($error);
                        }
                    }
                }

                $category->unsetData('use_post_data_config');

                $category->save();

                return $category;
            }
            catch (Exception $e){
                $this->err = $e->getMessage();
                return false;
//                var_dump($e->getMessage());
//                $this->_getSession()->addError($e->getMessage())->setCategoryData($data);
            }
        } catch(Exception $e) {
            $this->err = $e->getMessage();
            return false;
//            var_dump($e);
        }
    }
    protected function _get_product($produs) {
        $this->err = NULL;

        $product = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('sku', $produs->sku)
            ->getFirstItem()    // Assuming your category names are unique ??
        ;
        if (null !== $product->getId()) {
            return true;
        } else {
            return false;
        }
    }
    protected function _add_product($produs, $category) {
        $this->err = NULL;

        $product = Mage::getModel('catalog/product');
        try{
            $product
                ->setStoreId($this->storeId) //you can set data in store scope
                ->setWebsiteIds(array($this->websiteId)) //website ID the product is assigned to, as an array
                ->setAttributeSetId($this->attributeSetId) //ID of a attribute set named 'default'
                ->setTypeId('simple') //product type
                ->setCreatedAt(strtotime('now')) //product creation time
//    ->setUpdatedAt(strtotime('now')) //product update time

                ->setSku($produs->sku) //SKU
                ->setName($produs->nume) //product name
//                        ->setWeight(4.0000)
                ->setStatus(1) //product status (1 - enabled, 2 - disabled)
                ->setTaxClassId(4) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
//                        ->setManufacturer(28) //manufacturer id
//                        ->setColor(24)
//                        ->setNewsFromDate('06/26/2014') //product set as new from
//                        ->setNewsToDate('06/30/2014') //product set as new to
//                        ->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)

                ->setPrice($produs->pret) //price in form 11.22
                ->setCost($produs->pret) //price in form 11.22
//                        ->setSpecialPrice(00.44) //special price in form 11.22
//                        ->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
//                        ->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
//                        ->setMsrpEnabled(1) //enable MAP
//                        ->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
//                        ->setMsrp(99.99) //Manufacturer's Suggested Retail Price

                ->setMetaTitle($produs->nume)
                ->setMetaKeyword($produs->nume)
                ->setMetaDescription($produs->nume)

                ->setDescription($produs->nume.' long description')
                ->setShortDescription($produs->nume.' short description')

//                        ->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
//                        ->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery

                ->setStockData(array(
                        'use_config_manage_stock' => 0, //'Use config settings' checkbox
                        'manage_stock'=>1, //manage stock
                        'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
                        'max_sale_qty'=>2, //Maximum Qty Allowed in Shopping Cart
                        'is_in_stock' => 1, //Stock Availability
                        'qty' => 999 //qty
                    )
                )

                ->setCategoryIds(array($category->getId())); //assign product to categories
            $product->save();
//endif;
            return true;
        }catch(Exception $e){
            $this->err = $e->getMessage();
            return false;
//            var_dump($e->getMessage());
        }
    }
    protected function _add_log() {
        $this->err = NULL;

        $data = array(
            'name' => $this->file_name,
            'produse' => 10,
            'produse_importate' => 8,
            'produse_neimportate' => 2);

        $log = Mage::getModel('bteodor_importproduse/import')->addData($data);
        try {
            $log->save(); //save data
            return true;
//            $insertId = $log->getId();
//            echo "Data successfully inserted. Insert ID: " . $insertId;
        } catch (Exception $e) {
            $this->err = $e->getMessage();
            return false;
        }
    }
    private function _error($msg) {
        if($this->err !== NULL) Mage::throwException($this->err);
        Mage::throwException($msg);
    }
}
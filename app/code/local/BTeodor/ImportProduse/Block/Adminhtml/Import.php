<?php
class BTeodor_ImportProduse_Block_Adminhtml_Import
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        parent::_construct();
        
        /**
         * The $_blockGroup property tells Magento which alias to use to
         * locate the blocks to be displayed within this grid container.
         * In our example this corresponds to ImportDirectory/Block/Adminhtml.
         */
        $this->_blockGroup = 'bteodor_importproduse_adminhtml';
        
        /**
         * $_controller is a bit of a confusing name for this property. This 
         * value in fact refers to the folder containing our Grid.php and 
         * Edit.php. In our example, ImportDirectory/Block/Adminhtml/Import,
         * so we use 'import'.
         */
        $this->_controller = 'import';
        
        /**
         * The title of the page in the admin panel.
         */
        $this->_headerText = Mage::helper('bteodor_importproduse')
            ->__('Import Log');
    }
    
    public function getCreateUrl()
    {
        /**
         * When the Add button is clicked, this is where the user should
         * be redirected to. In our example, the method editAction of 
         * ImportController.php in ImportDirectory module.
         */
        return $this->getUrl(
            'bteodor_importproduse_admin/import/edit'
        );
    }
}
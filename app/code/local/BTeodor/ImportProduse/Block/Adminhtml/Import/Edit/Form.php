<?php
class BTeodor_ImportProduse_Block_Adminhtml_Import_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        // instantiate a new form to display our import for editing
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl(
                'bteodor_importproduse_admin/import/edit',
                array(
                    '_current' => true,
                    'continue' => 0,
                )
            ),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $form->setUseContainer(true);
        $this->setForm($form);
        
        // define a new fieldset, we only need one for our simple entity
        $fieldset = $form->addFieldset(
            'general',
            array(
                'legend' => $this->__('Import')
            )
        );
        
        $importSingleton = Mage::getSingleton(
            'bteodor_importproduse/import'
        );
        $fieldset->addField('fisier', 'file', array(

            'label'     => $this->__('Fisier'),
            'required'  => true,
            'name'      => 'fisier'

));


        return $this;
    }
    
    /**
     * This method makes life a little easier for us by pre-populating 
     * fields with $_POST data where applicable and wraps our post data in 
     * 'importData' so we can easily separate all relevant information in
     * the controller. You can of course omit this method entirely and call
     * the $fieldset->addField() method directly.
     */
    protected function _addFieldsToFieldset(
        Varien_Data_Form_Element_Fieldset $fieldset, $fields)
    {
        $requestData = new Varien_Object($this->getRequest()
            ->getPost('importData'));
        
        foreach ($fields as $name => $_data) {
            if ($requestValue = $requestData->getData($name)) {
                $_data['value'] = $requestValue;
            }
            
            // wrap all fields with importData group
            $_data['name'] = "importData[$name]";
            
            // generally label and title always the same
            $_data['title'] = $_data['label'];
            
            // if no new value exists, use existing import data
            if (!array_key_exists('value', $_data)) {
                $_data['value'] = $this->_getImport()->getData($name);
            }
            
            // finally call vanilla functionality to add field
            $fieldset->addField($name, $_data['input'], $_data);
        }
        
        return $this;
    }
    
    /**
     * Retrieve the existing import for pre-populating the form fields.
     * For a new import entry this will return an empty Import object.
     */
    protected function _getImport()
    {
        if (!$this->hasData('import')) {
            // this will have been set in the controller
            $import = Mage::registry('current_import');
            
            // just in case the controller does not register the import
            if (!$import instanceof
                    BTeodor_ImportProduse_Model_Import) {
                $import = Mage::getModel(
                    'bteodor_importproduse/import'
                );
            }
            
            $this->setData('import', $import);
        }
        
        return $this->getData('import');
    }
}
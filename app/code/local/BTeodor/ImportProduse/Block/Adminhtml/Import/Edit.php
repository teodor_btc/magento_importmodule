<?php
class BTeodor_ImportProduse_Block_Adminhtml_Import_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'bteodor_importproduse_adminhtml';
        $this->_controller = 'import';
        
        /**
         * The $_mode property tells Magento which folder to use to
         * locate the related form blocks to be displayed within this
         * form container. In our example this corresponds to 
         * ImportDirectory/Block/Adminhtml/Import/Edit/.
         */
//        $this->_mode = 'edit';
//
//        $newOrEdit = $this->getRequest()->getParam('id')
//            ? $this->__('Edit')
//            : $this->__('New');
//        $this->_headerText =  $newOrEdit . ' ' . $this->__('Import');
        $this->_headerText =  $this->__('Import');
    }
}
<?php
class BTeodor_ImportProduse_Block_Adminhtml_Import_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _prepareCollection()
    {
        /**
         * Tell Magento which Collection to use for displaying in the grid.
         */
        $collection = Mage::getResourceModel(
            'bteodor_importproduse/import_collection'
        );
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
//    public function getRowUrl($row)
//    {
//        /**
//         * When a grid row is clicked, this is where the user should
//         * be redirected to. In our example, the method editAction of
//         * ImportController.php in ImportDirectory module.
//         */
//        return $this->getUrl(
//            'bteodor_importproduse_admin/import/edit',
//            array(
//                'id' => $row->getId()
//            )
//        );
//    }

    protected function _prepareColumns()
    {
        /**
         * Here we define which columns we want to be displayed in the grid.
         */
        $this->addColumn('entity_id', array(
            'header' => $this->_getHelper()->__('ID'),
            'type' => 'number',
            'index' => 'entity_id',
        ));
        
        $this->addColumn('created_at', array(
            'header' => $this->_getHelper()->__('Data import'),
            'type' => 'datetime',
            'index' => 'created_at',
        ));
        
        $this->addColumn('name', array(
            'header' => $this->_getHelper()->__('Fisier'),
            'type' => 'text',
            'index' => 'name',
        ));
        
        $this->addColumn('produse', array(
            'header' => $this->_getHelper()->__('Produse'),
            'type' => 'text',
            'index' => 'produse',
        ));

        $this->addColumn('produse_importate', array(
            'header' => $this->_getHelper()->__('Produse Importate'),
            'type' => 'text',
            'index' => 'produse_importate'
        ));

        $this->addColumn('produse_neimportate', array(
            'header' => $this->_getHelper()->__('Produse Neimportate'),
            'type' => 'text',
            'index' => 'produse_neimportate'
        ));

        return parent::_prepareColumns();
    }
    
    protected function _getHelper()
    {
        return Mage::helper('bteodor_importproduse');
    }
}